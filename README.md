> Project started at 7/9/2021 July (18 Tir 1400) 

# Car Park Finder #

### Introduction ###

### Now-days our cities are crowded and It is a concern to park your car in a appropriate place, If you have a car, you would park it easily in addition you could book your place by this solution. ###

<img src="Src/Image1.jpg" width="6000" />

*What are we going to do?*

1. **Consider and analyze the relevant markets**
2. **Note down your ideas and expand them**
3. **Carry out competitive analysis**
4. **Model your business**
5. **Create/design/sketch your mock-up and then test it**
6. **Execute a market survey**
7. **Develop your final product**
8. **Gathering customer feedback and measuring your business results**

*See details:*
[**HOW TO DEVELOP YOUR START-UP IDEA**](https://canvanizer.com/how-to-use/how-to-develop-your-startup-idea)

This project is going to develop on smart phones so we should develop an application which is compatible with all smart phones in addition it is based on IOS and Android, The first phase of project is to develop an Android App.

### Stage 1 : Consider and analyze the relevant markets ###

The whole people who have a car could use this App however these people can be classified to:

1. Person who wants to park his car
2. Person who has a space for others people wanting a space for parking (this space is called a **parking lot** or **car park** or a **car lot**

Besides the public parking places also are shared with this App.

### Stage 2 : Note down your ideas and expand them ###

To identify parking lots which is the first step and the most important one we should find good ideas, the simplest one is to identify parking lots via the people who have the App for example at the first time which the parking lots are not known, people park their cars in parking lots and App can recolonize them but this idea has some profs and cons.


###### profs:

This algorithm recolonize the car lots so quickly and is so simple to implement.

###### cons:

1. This needs to initialize the car lots by first App running.
2. This algorithm consists of human faults such as detecting forbidden parking spaces
3. Updating state is weak because some spaces may lead to forbid
  
Another idea is to employ specific people to recognize parking spaces and lead cars to these spaces this idea is good but has cost.

Also there are some methods to do this job that are classified to vision-based and non vision-based which are very expensive and we try to not approach these methods.	

### Stage 3 : Carry out competitive analysis ###

There are two competitive markets:

- [Alopark](https://play.google.com/store/apps/details?id=com.aloparkapp&gl=DE)
- [ja Park](https://cafebazaar.ir/app/com.fahim.japark?l=en)

##### Alopark

It's available on google play, This app is not a real competitor market because it just support public parking places and it just a online reservation.

##### ja Park

It's available on cafebazar, This app is very near to our project but it's not viral enough because it seems that the owners did not investigate enough for this app besides this app have so many faults to run and execute. The last comments for this app depends to about 4 years ago so it's inactive enough but it's a good one to take pattern from it. 

### Stage 4 : Model your business ###
